FROM php:7.1.0-apache
COPY php.ini /usr/local/etc/php/
COPY apache2.conf /etc/apache2/apache2.conf
RUN a2enmod rewrite
RUN apt-get update

# Install Basic packages
RUN apt-get install -y libpq-dev libzip-dev libicu-dev wget zip unzip vim

# Install bz2
RUN apt-get install -y libbz2-dev
RUN docker-php-ext-install bz2

# Install mbstring
RUN docker-php-ext-install mbstring

# Install mcrypt
RUN apt-get install -y libmcrypt-dev
RUN docker-php-ext-install mcrypt

# Install intl
RUN pecl install intl
RUN docker-php-ext-install intl

# Install pdo_pgsql
RUN docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql
RUN docker-php-ext-install pdo pdo_pgsql pdo_mysql exif pgsql

# Install php-gd
RUN apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng12-dev
RUN docker-php-ext-install -j$(nproc) iconv \
&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir==/usr/include/ \
&& docker-php-ext-install -j$(nproc) gd
# Install composer
## Register the COMPOSER_HOME environment variable
ENV COMPOSER_HOME /composer
## Add global binary directory to PATH and make sure to re-export it
ENV PATH /composer/vendor/bin:$PATH
## Allow Composer to be run as root
ENV COMPOSER_ALLOW_SUPERUSER 1
## Setup the Composer installer
RUN curl -o /tmp/composer-setup.php https://getcomposer.org/installer \
  && curl -o /tmp/composer-setup.sig https://composer.github.io/installer.sig \
  && php -r "if (hash('SHA384', file_get_contents('/tmp/composer-setup.php')) !== trim(file_get_contents('/tmp/composer-setup.sig'))) { unlink('/tmp/composer-setup.php'); echo 'Invalid installer' . PHP_EOL; exit(1); }"
RUN php /tmp/composer-setup.php --no-ansi --install-dir=/usr/local/bin --filename=composer && rm -rf /tmp/composer-setup.php
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install php extension zip
RUN docker-php-ext-install zip

# Install git
RUN apt-get install -y libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev
RUN apt-get install -y git

#RUN cd /usr/local/bin
#RUN wget http://download.gna.org/wkhtmltopdf/0.12/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
#RUN xz -dv wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
#RUN tar xfv wkhtmltox-0.12.4_linux-generic-amd64.tar.xz
#RUN cd wkhtmltox/bin
#RUN mv * /usr/local/bin/

RUN apt-get install -y fonts-ipafont-gothic fonts-ipafont-mincho
RUN apt-get install -y libfontconfig1 libxrender1
ADD wkhtmltox-0.12.4_linux-generic-amd64.tar.xz /usr/local/bin/
RUN cp /usr/local/bin/wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf

#RUN wget https://downloads.wkhtmltopdf.org/0.12/0.12.4/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz -P /usr/local/bin/
#RUN xz -dc /usr/local/bin/wkhtmltox-0.12.4_linux-generic-amd64.tar.xz | tar xfv -
#RUN cp /usr/local/bin/wkhtmltox/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf

# Install cakephp
#RUN cd /var/www/html
# RUN composer config -g repositories.packagist composer https://packagist.jp
#RUN composer create-project --prefer-dist cakephp/app freoncheck

# RUN apt-get install locales locales-all
# RUN locale-gen en_US.UTF-8
ENV LANG ja_JP.UTF-8
# ENV LANGUAGE en_US:en
# ENV LC_ALL en_US.UTF-8

ENV TZ Asia/Tokyo

EXPOSE 80

